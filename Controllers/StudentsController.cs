﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DormitoryManagmer.Models;
using DormitoryManagmer.Services;

namespace DormitoryManagmer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        private readonly StudentsService _studentsService;

        public StudentsController(StudentsService studentsService)
        {
            _studentsService = studentsService;
        }
        // GET
        [HttpGet]
        public ActionResult<List<Students>> Get() =>
            _studentsService.Get();
        [HttpGet("{id:length(24)}")]
        public ActionResult<Students> Get(string id)
        {
            var student = _studentsService.Get(id);
            if(student == null)
            {
                return NotFound();
            }
            return student;
        }
        // POST
        [HttpPost]
        public ActionResult<Students> Create(Students student)
        {
            _studentsService.Create(student);
            return CreatedAtRoute(new { id = student.Id.ToString() }, student);
        }
        // PUT
        [HttpPut("{id:length(24)}")]
        public ActionResult<Students> Update(string id, Students student)
        {
            var stud = _studentsService.Get(id);
            if (stud == null)
            {
                return NotFound();
            }
            _studentsService.Update(id, student);
            return NoContent();
        }
        // DELETE
        [HttpDelete("{id:length(24)}")]
        public ActionResult<Students> Remove(string id)
        {
            var stud = _studentsService.Get(id);
            if (stud == null)
            {
                return NotFound();
            }
            _studentsService.Remove(id);
            return NoContent();
        }
    }
}