﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using DormitoryManagmer.Models;

namespace DormitoryManagmer.Services
{
    public class StudentsService
    {
        private readonly IMongoCollection<Students> _students;
        // connetion to mongo
        public StudentsService(IStudentsStoreDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            _students = database.GetCollection<Students>(settings.StudentsCollectionName);
            
        }
        
        // GET request
        public List<Students> Get() =>
            _students.Find(student => true).ToList();

        public Students Get(string id) =>
            _students.Find(student => student.Id == id).FirstOrDefault();

        // POST request
        public Students Create(Students student)
        {
            _students.InsertOne(student);
            return student;
        }
        
        // PUT request
        public void Update(string id, Students student) =>
            _students.ReplaceOne(stud => stud.Id == id, student);
        
        // DELETE request
        public void Remove(string id) =>
            _students.DeleteOne(stud => stud.Id == id);
    }




}
